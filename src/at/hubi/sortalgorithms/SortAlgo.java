package at.hubi.sortalgorithms;

public interface SortAlgo {
	public int[] sort(int[] data);

	public String getName();
}
