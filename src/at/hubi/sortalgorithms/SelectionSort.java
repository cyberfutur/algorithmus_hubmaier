package at.hubi.sortalgorithms;

import at.hubi.algorithms.MainClass;

public class SelectionSort implements SortAlgo {

	@Override
	public int[] sort(int[] input) {
		// DataGenerator.printData(input);
		int minimum = MainClass.min;
		int size = 0;
		while (minimum != MainClass.max + 1) {
			for (int z = input.length - 1; z >= size; z--) {
				if (input[z] <= minimum) {
					int temp = input[size];
					input[size] = input[z];
					input[z] = temp;
					size++;
					// System.out.println("Size:" + size);
					// System.out.println("Sorted out a " + minimum);
				}

			}
			minimum++;
		}
		for (int q = 0; q < input.length - 1; q++) {
			if (input[q] > input[q + 1]) {
				int temp1 = input[q];
				input[q] = input[q + 1];
				input[q + 1] = temp1;
			}
		}
		return input;
	}

	@Override
	public String getName() {
		return "SelectionSort";
	}

}
