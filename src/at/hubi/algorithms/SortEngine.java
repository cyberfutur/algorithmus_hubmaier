package at.hubi.algorithms;

import at.hubi.sortalgorithms.SortAlgo;

public class SortEngine {
	private SortAlgo sortAlgo;

	public int[] sort(int[] data) {
		return sortAlgo.sort(data);
	}

	public void printResult(int[] ls) {
		System.out.println("Sorted List:");
		for (int i = 0; i < ls.length; i++) {
			System.out.println(ls[i] + " ");
		}
	}

	public SortAlgo getSortAlgo() {
		return sortAlgo;
	}

	public void setSortAlgo(SortAlgo sortAlgo) {
		this.sortAlgo = sortAlgo;
	}

}
