package at.hubi.algorithms;

import at.hubi.algorithms.helper.DataGenerator;
import at.hubi.algorithms.helper.SpeedTest;
import at.hubi.sortalgorithms.BallSorter;
import at.hubi.sortalgorithms.BubbleSort;
import at.hubi.sortalgorithms.InsertionSort;
import at.hubi.sortalgorithms.SelectionSort;
import at.hubi.sortalgorithms.SortAlgo;

public class MainClass {
	public static int min = 1;
	public static int max = 30;

	public static void main(String[] args) {

		int[] sort = DataGenerator.generateRandomData(2000, min, max);
		SortAlgo sas = new SelectionSort();
		SortAlgo sab = new BubbleSort();
		SortAlgo sab1 = new BallSorter();
		SortAlgo sai = new InsertionSort();
		/*
		 * SortEngine se = new SortEngine(); se.setSortAlgo(sai);
		 * se.printResult(se.sort(sort));
		 */
		SpeedTest st = new SpeedTest();
		st.addAlgorithm(sas);
		st.addAlgorithm(sab);
		st.addAlgorithm(sab1);
		st.addAlgorithm(sai);
		st.run();

	}
}
