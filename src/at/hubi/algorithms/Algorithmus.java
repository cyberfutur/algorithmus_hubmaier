package at.hubi.algorithms;

import java.util.LinkedList;

import at.hubi.algorithms.helper.DataGenerator;

public class Algorithmus {

	public int[] sort(int[] input) {
		LinkedList<Integer> result = new LinkedList<Integer>();
		DataGenerator.printData(input);
		// Collections.sort(result);
		boolean first = false;
		int compare = 0;
		// ArrayList<Integer> result = new ArrayList<Integer>();
		for (int i : input) {
			if (first == false) {
				result.add(i);
				compare = i;
				first = true;
			} else if (first == true) {
				if (i == compare) {
					result.add(result.indexOf(compare), i);
					// System.out.println("Same");
				} else if (i > compare) {
					compare = i;
					result.addLast(i);
					// System.out.println("Bigger");
				} else if (i < compare) {
					// System.out.println("Smaller");
					int current = result.size();
					for (int p = 0; p < current; p++) {
						if (result.get(p) > i) {
							result.add(result.indexOf(result.get(p)), i);
							break;
						}
					}
				}
			}
		}
		int[] resultArray = new int[result.size()];
		for (int i = 0; i < resultArray.length; i++) {
			resultArray[i] = result.get(i);
		}
		return resultArray;
	}

	public int[] bubbleSort(int[] input) {
		DataGenerator.printData(input);
		for (int i = 0; i < input.length; i++) {
			int count = 0;
			if ((i + 1) < input.length) {
				for (int j = 1; j < input.length; j++) {
					if (input[count] > input[j]) {
						int temp = input[j];
						input[j] = input[count];
						input[count] = temp;
						count++;
					} else {
						count++;
					}
				}
			}

		}
		return input;
	}

	public class SelectionSort {
		private int[] a;

		public void sort(int[] anArray) {
			a = anArray;
			selectionSort();
		}

		private void selectionSort() {
			for (int i = 0; i < a.length - 1; i++) {
				int minposition = minimumPosition(i);
				swap(minposition, i);
			}
		}

		private int minimumPosition(int from) {
			int minposition = from;
			for (int i = from + 1; i < a.length; i++)
				if (a[i] < a[minposition])
					minposition = i;
			return minposition;
		}

		private void swap(int i, int j) {
			int temp = a[i];
			a[i] = a[j];
			a[j] = temp;
		}
	}

	public void printResult(int[] ls) {
		System.out.println("Sorted List:");
		for (int i = 0; i < ls.length; i++) {
			System.out.println(ls[i] + " ");
		}
	}
}
