package at.hubi.searchalgorithms;

public class NumberNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NumberNotFoundException(String message) {
		super(message);
	}
}
