package at.hubi.searchalgorithms;

public interface SearchAlgorithms {

	public int Search(int[] a, int s) throws NumberNotFoundException;

}