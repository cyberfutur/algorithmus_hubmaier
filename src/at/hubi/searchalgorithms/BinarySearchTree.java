package at.hubi.searchalgorithms;

import java.util.ArrayList;
import java.util.Collections;

public class BinarySearchTree implements SearchAlgorithms {

	@Override
	public int Search(int[] a, int s) throws NumberNotFoundException {
		ArrayList<Integer> search = new ArrayList<Integer>();
		int pos = 0;
		boolean found = false;
		for (int i = 0; i < a.length; i++) {
			search.add(a[i]);
			System.out.println(a[i]);
		}
		Collections.sort(search);
		int mid = (search.size() - 1) / 2;
		while (found == false && mid > 1) {
			int diffHigh = (search.size() - 1) - mid;
			if (search.get(mid) == s) {
				pos = mid;
				found = true;
			} else if (search.get(mid) < s) {
				mid = mid / 2;
			} else if (search.get(mid) > s) {
				mid = mid + (diffHigh / 2);
			}
		}
		if (found == false) {
			throw new NumberNotFoundException("Net do!");
		}
		/*
		 * while(search.size() != 1){ int compare =
		 * search.get((search.size()-1)/2); if(compare > s){ for(int z =
		 * (search.size()-1)/2;z < search.size();z++){ search.remove(z); }
		 * counter++; } else if(compare < s){ for(int q = 0;q <
		 * search.size()/2;q++){ search.remove(q); } counter++; } else
		 * if(compare == s){ pos = (int) (search.indexOf(compare)*Math.pow(2,
		 * counter)); break; } }
		 */
		return pos;

	}

}
